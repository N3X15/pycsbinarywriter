# 0.1.2 - July 19, 2024

* Update dependencies

# 0.1.1 - June 30, 2023

* Fixed large int7 values.

# 0.1.0 - June 23, 2023

* Updated dependencies
* General code cleanup
* Added: asyncio support (via overloads)
* Added: decimal encoding support

# 0.0.6 - April 25, 2023

* Updated and trimmed dependencies

# 0.0.4 - October 6, 2022

* Updated dependencies
* VERSION actually reflects package version number

# 0.0.3 - September 9, 2022

* Updated dependencies

# 0.0.2 - June 15, 2022

* Removed Numpy dependency; ctypes is now used for some internal bitwise math instead.
* Fixed invalid import crash
* Code cleanup with autopep8 and isort

# 0.0.1

Initial release