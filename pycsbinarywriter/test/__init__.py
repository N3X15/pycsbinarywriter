import unittest
from .simple import TestSimpleSerializers
from .strings import TestStringSerializers
from .int7_regressions import TestInt7Regressions

if __name__ == '__main__':
    unittest.main()
