﻿using System;
using System.IO;
namespace NumGen
{
    class Program
    {
        static byte[] toBytes(bool v) {
            using(var ms = new MemoryStream()) {
                using(var w = new BinaryWriter(ms)){
                    w.Write(v);
                }
                ms.Flush();
                return ms.ToArray();
            }
        }
        static byte[] toBytes(byte v) {
            using(var ms = new MemoryStream()) {
                using(var w = new BinaryWriter(ms)){
                    w.Write(v);
                }
                ms.Flush();
                return ms.ToArray();
            }
        }
        static byte[] toBytes(sbyte v) {
            using(var ms = new MemoryStream()) {
                using(var w = new BinaryWriter(ms)){
                    w.Write(v);
                }
                ms.Flush();
                return ms.ToArray();
            }
        }
        static byte[] toBytes(ushort v) {
            using(var ms = new MemoryStream()) {
                using(var w = new BinaryWriter(ms)){
                    w.Write(v);
                }
                ms.Flush();
                return ms.ToArray();
            }
        }
        static byte[] toBytes(short v) {
            using(var ms = new MemoryStream()) {
                using(var w = new BinaryWriter(ms)){
                    w.Write(v);
                }
                ms.Flush();
                return ms.ToArray();
            }
        }
        static byte[] toBytes(uint v) {
            using(var ms = new MemoryStream()) {
                using(var w = new BinaryWriter(ms)){
                    w.Write(v);
                }
                ms.Flush();
                return ms.ToArray();
            }
        }
        static byte[] toBytes(int v) {
            using(var ms = new MemoryStream()) {
                using(var w = new BinaryWriter(ms)){
                    w.Write(v);
                }
                ms.Flush();
                return ms.ToArray();
            }
        }
        static byte[] toBytes(ulong v) {
            using(var ms = new MemoryStream()) {
                using(var w = new BinaryWriter(ms)){
                    w.Write(v);
                }
                ms.Flush();
                return ms.ToArray();
            }
        }
        static byte[] toBytes(long v) {
            using(var ms = new MemoryStream()) {
                using(var w = new BinaryWriter(ms)){
                    w.Write(v);
                }
                ms.Flush();
                return ms.ToArray();
            }
        }
        static byte[] toBytes(float v) {
            using(var ms = new MemoryStream()) {
                using(var w = new BinaryWriter(ms)){
                    w.Write(v);
                }
                ms.Flush();
                return ms.ToArray();
            }
        }
        static byte[] toBytes(double v) {
            using(var ms = new MemoryStream()) {
                using(var w = new BinaryWriter(ms)){
                    w.Write(v);
                }
                ms.Flush();
                return ms.ToArray();
            }
        }
        static byte[] toBytes(string v) {
            using(var ms = new MemoryStream()) {
                using(var w = new BinaryWriter(ms)){
                    w.Write(v);
                }
                ms.Flush();
                return ms.ToArray();
            }
        }
        static byte[] toBytes(char v) {
            using(var ms = new MemoryStream()) {
                using(var w = new BinaryWriter(ms)){
                    w.Write(v);
                }
                ms.Flush();
                return ms.ToArray();
            }
        }
        static string toPythonBytes(bool v) { return toPythonBytesInternal(toBytes(v),v); }
        static string toPythonBytes(byte v) { return toPythonBytesInternal(toBytes(v),v); }
        static string toPythonBytes(sbyte v) { return toPythonBytesInternal(toBytes(v),v); }
        static string toPythonBytes(ushort v) { return toPythonBytesInternal(toBytes(v),v); }
        static string toPythonBytes(short v) { return toPythonBytesInternal(toBytes(v),v); }
        static string toPythonBytes(uint v) { return toPythonBytesInternal(toBytes(v),v); }
        static string toPythonBytes(int v) { return toPythonBytesInternal(toBytes(v),v); }
        static string toPythonBytes(ulong v) { return toPythonBytesInternal(toBytes(v),v); }
        static string toPythonBytes(long v) { return toPythonBytesInternal(toBytes(v),v); }
        static string toPythonBytes(float v) { return toPythonBytesInternal(toBytes(v),v); }
        static string toPythonBytes(double v) { return toPythonBytesInternal(toBytes(v),v); }
        static string toPythonBytes(string v) { return toPythonBytesInternal(toBytes(v),v); }
        static string toPythonBytes(char v) { return toPythonBytesInternal(toBytes(v),v); }
        static string toPythonBytesInternal(byte[] csbytes, object v) {
            var pybytes = "";
            for (int i = 0; i < csbytes.Length; i++)
            {
                pybytes += String.Format("\\x{0:x2}", csbytes[i]);
            }
            return String.Format("{0}: {{ value: {1}, bytes: b'{2}' }}", v.GetType().Name.ToLower(), v, pybytes);
        }
        static void Main(string[] args)
        {
            Console.WriteLine(toPythonBytes(true));
            Console.WriteLine(toPythonBytes((byte)127));
            Console.WriteLine(toPythonBytes((sbyte)-127));
            Console.WriteLine(toPythonBytes((ushort)1234));
            Console.WriteLine(toPythonBytes((short)-1234));
            Console.WriteLine(toPythonBytes((uint)1234567));
            Console.WriteLine(toPythonBytes((int)-1234567));
            Console.WriteLine(toPythonBytes((ulong)1234567890));
            Console.WriteLine(toPythonBytes((long)-1234567890));

            Console.WriteLine(toPythonBytes((float)1.5));
            Console.WriteLine(toPythonBytes((double)1.0000005));

            Console.WriteLine(toPythonBytes("abc123"));
            Console.WriteLine(toPythonBytes('a'));
        }
    }
}
