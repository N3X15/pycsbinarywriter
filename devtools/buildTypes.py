import ast
from datetime import datetime, timezone
from typing import List

import black
import isort
import toml
from mkast.formatters.autoflake import AutoflakeFormattingProvider
from mkast.formatters.black import BlackFormattingProvider
from mkast.formatters.isort import ISortFormattingProvider
from mkast.generators.astor import AstorCodeGenerator
from mkast.modulefactory import ModuleFactory
from mkast.utils import value2ast
from ruamel.yaml import YAML

yaml = YAML(typ='rt')


def pytype2ast(pytype: str) -> ast.AST:
    match pytype:
        case 'builtins.int':
            return ast.Attribute(
                value=ast.Name('builtins', ctx=ast.Load()),
                attr='int'
            )
        case 'builtins.float':
            return ast.Attribute(
                value=ast.Name('builtins', ctx=ast.Load()),
                attr='float'
            )
        case 'builtins.bool':
            return ast.Attribute(
                value=ast.Name('builtins', ctx=ast.Load()),
                attr='bool'
            )


class PyCSCodeGenerator(AstorCodeGenerator):
    def __init__(self) -> None:
        super().__init__()
        cfg = isort.Config(line_length=65355,
                           ignore_whitespace=True,
                           quiet=True,
                           lines_after_imports=0,
                           lines_before_imports=0,
                           lines_between_sections=0,
                           lines_between_types=0)
        self.addFormatter(ISortFormattingProvider(cfg))
        self.addFormatter(AutoflakeFormattingProvider(
            remove_all_unused_imports=True))
        self.addFormatter(BlackFormattingProvider(
            black.Mode(line_length=65355)))


def clean_code_str(cstr: str) -> str:
    tree = ast.parse(cstr)
    return PyCSCodeGenerator().generate(tree)

# pwd = Path.cwd()
# print('Building numgen...')
# subprocess.check_call(['dotnet', 'build'], cwd=pwd / 'devtools' / 'numgen')
# NUMGEN_PATH = pwd / 'devtools' / 'numgen' / 'bin' / ('NumGen'+('.exe' if os.name == 'nt' else ''))
# o = subprocess.check_output([NUMGEN_PATH], executable=NUMGEN_PATH)


data = []
with open('data/types.yml', 'r') as f:
    data = yaml.load(f)

with open('pycsbinarywriter/cstypes/simple.py', 'w') as w:
    w.write(f'# @GENERATED by build/buildTypes.py (see data/types.yml)\n')
    w.write(f'# Generated at {datetime.now(tz=timezone.utc).isoformat()}\n')
    mf = ModuleFactory()
    mf.addImport('builtins')
    # mf.addImport('sys')
    mf.addImportFrom('pycsbinarywriter.cstypes.cstype', ['SimpleCSType'])
    all_ = sorted(list(data.keys()))
    mf.addVariableDecl('__all__', None,
                       ast.List([
                           ast.Constant(x) for x in all_
                       ]))

    for typeid, typedata in data.items():
        fmt = typedata['fmt']
        size = typedata['size']
        pytype = typedata['pytype']
        # w.writeline(
        #     f'{typeid}: SimpleCSType[{pytype}] = SimpleCSType({typeid!r}, {fmt!r}, {size!r}, {pytype})')
        mf.addVariableDecl(
            name=typeid,
            annotation=ast.Subscript(
                value=ast.Name('SimpleCSType', ctx=ast.Load()),
                slice=ast.Index(pytype2ast(pytype))
            ),
            value=ast.Call(
                func=ast.Name('SimpleCSType', ctx=ast.Load()),
                args=[
                    ast.Constant(typeid),
                    ast.Constant(fmt),
                    ast.Constant(size),
                    pytype2ast(pytype),
                ],
                keywords=[],
            )
        )
        for alias in typedata.get('aliases', []):
            mf.addVariableDecl(
                name=alias,
                annotation=ast.Subscript(
                    value=ast.Name('SimpleCSType', ctx=ast.Load()),
                    slice=pytype2ast(pytype)
                ),
                value=ast.Name(typeid, ctx=ast.Load())
            )

    w.write(PyCSCodeGenerator().generate(mf.generate()))

with open('pycsbinarywriter/test/simple.py', 'w') as w:
    w.write(f'# @GENERATED by build/buildTypes.py (see data/types.yml)\n')
    w.write(f'# Generated at {datetime.now(tz=timezone.utc).isoformat()}\n')
    mf = ModuleFactory()
    mf.addImport('builtins')
    mf.addImport('unittest')
    mf.addImportFrom('pycsbinarywriter', ['cstypes'])
    mf.addVariableDecl('__all__', None, ast.List(
        elts=[ast.Str('TestSimpleSerializers')]
    ))

    cls = ast.ClassDef(
        name='TestSimpleSerializers',
        bases=[
            ast.Attribute(
                value=ast.Name('unittest', ctx=ast.Load()),
                attr='TestCase')
        ],
        body=[],
        keywords=[],
        decorator_list=[],
        starargs=None,
        kwargs=None
    )
    mf.expressions.append(cls)
    for typeid, typedata in data.items():
        fmt = typedata['fmt']
        size = typedata['size']
        pytype = typedata['pytype']
        t = typedata['tests']
        value = t['value']
        byteval = ast.literal_eval('b"'+t['bytes']+'"')
        # test pack
        sig = ast.arguments(
            posonlyargs=[],
            args=[
                ast.arg(arg='self', annotation=None, type_comment=None),
            ],
            vararg=None,
            kwonlyargs=[],
            kw_defaults=[],
            kwarg=None,
            defaults=[]
        )
        testpack = ast.FunctionDef(
            name=f'test_{typeid}_pack',
            args=sig,
            body=[],
            decorator_list=[],
            returns=ast.Constant(None),
        )
        cls.body.append(testpack)
        testpack.body.append(
            ast.AnnAssign(
                target=ast.Name('input_value', ctx=ast.Store()),
                value=value2ast(value),
                annotation=pytype2ast(pytype),
                simple=1
            )
        )
        testpack.body.append(
            ast.AnnAssign(
                target=ast.Name('expected', ctx=ast.Store()),
                value=value2ast(byteval),
                annotation=ast.Name('bytes', ctx=ast.Load()),
                simple=1
            )
        )
        testpack.body.append(
            ast.AnnAssign(
                target=ast.Name('actual', ctx=ast.Store()),
                value=ast.Call(
                    func=ast.Attribute(
                        value=ast.Attribute(
                            value=ast.Name('cstypes', ctx=ast.Load()),
                            attr=typeid
                        ),
                        attr='pack'
                    ),
                    args=[
                        ast.Name('input_value', ctx=ast.Load()),
                    ],
                    keywords=[],
                    starargs=None,
                    kwargs=None,
                ),
                annotation=ast.Name('bytes', ctx=ast.Load()),
                simple=1
            )
        )
        testpack.body.append(
            ast.Expr(
                ast.Call(
                    func=ast.Attribute(
                        value=ast.Name('self', ast.Load()),
                        attr='assertEqual'
                    ),
                    args=[
                        ast.arg('actual', None),
                        ast.arg('expected', None),
                    ],
                    keywords=[],
                    starargs=None,
                    kwargs=None,
                )
            )
        )
        # test pack
        sig = ast.arguments(
            posonlyargs=[],
            args=[
                ast.arg(arg='self', annotation=None, type_comment=None),
            ],
            vararg=None,
            kwonlyargs=[],
            kw_defaults=[],
            kwarg=None,
            defaults=[]
        )
        testunpack = ast.FunctionDef(
            name=f'test_{typeid}_unpack',
            args=sig,
            body=[],
            decorator_list=[],
            returns=ast.Constant(None),
        )
        cls.body.append(testunpack)
        testunpack.body.append(
            ast.AnnAssign(
                target=ast.Name('input_bytes', ctx=ast.Store()),
                value=value2ast(byteval),
                annotation=ast.Name('bytes', ctx=ast.Load()),
                simple=1
            )
        )
        testunpack.body.append(
            ast.AnnAssign(
                target=ast.Name('expected', ctx=ast.Store()),
                value=value2ast(value),
                annotation=pytype2ast(pytype),
                simple=1
            )
        )
        testunpack.body.append(
            ast.AnnAssign(
                target=ast.Name('actual', ctx=ast.Store()),
                value=ast.Call(
                    func=ast.Attribute(
                        value=ast.Attribute(
                            value=ast.Name('cstypes', ctx=ast.Load()),
                            attr=typeid
                        ),
                        attr='unpack'
                    ),
                    args=[
                        ast.arg('input_bytes', None),
                    ],
                    keywords=[],
                    starargs=None,
                    kwargs=None,
                ),
                annotation=pytype2ast(pytype),
                simple=1
            )
        )
        testunpack.body.append(
            ast.Expr(
                ast.Call(
                    func=ast.Attribute(
                        value=ast.Name('self', ast.Load()),
                        attr='assertEqual'
                    ),
                    args=[
                        ast.arg('actual', None),
                        ast.arg('expected', None),
                    ],
                    keywords=[],
                    starargs=None,
                    kwargs=None,
                )
            )
        )
    mf.expressions.append(
        ast.If(
            test=ast.Compare(
                ops=[ast.Eq()],
                left=ast.Name('__name__', ctx=ast.Load()),
                comparators=[ast.Constant('__main__')]
            ),
            body=[
                ast.Call(
                    func=ast.Attribute(
                        value=ast.Name('unittest', ctx=ast.Load()),
                        attr='main'
                    ),
                    args=[],
                    keywords=[],
                    starargs=None,
                    kwargs=None
                ),
            ],
            orelse=[]
        )
    )

    w.write(PyCSCodeGenerator().generate(mf.generate()))

pyproject = {}
with open('pyproject.toml', 'r') as f:
    pyproject = toml.load(f)
version_str: str = pyproject['tool']['poetry']['version']
version: List[int] = [int(x) for x in version_str.split('.')]
with open('pycsbinarywriter/consts.py', 'w') as w:
    w.write('# @GENERATED by build/buildTypes.py (see data/pyproject.toml)\n')
    mf = ModuleFactory()
    mf.addImportFrom('typing', ['Tuple'])
    ann = ast.Subscript(value=ast.Name('Tuple', ctx=ast.Load()),
                        slice=ast.Tuple([
                            ast.Name('int'), ast.Name('int'), ast.Name('int')]))
    mf.addVariableDecl('__version__', None, ast.Constant(version_str))
    mf.addVariableDecl('VERSION', ann, ast.Tuple(
        elts=[
            ast.Constant(version[0]),
            ast.Constant(version[1]),
            ast.Constant(version[2]),
        ]
    ))
    w.write(PyCSCodeGenerator().generate(mf.generate()))
